import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from './app.routing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { Login } from './components/login/login.component';
import { CampannasComponent } from './components/campannas/campannas.component';
import { PrincipalComponent } from './components/principal/principal.component';
import { ComponentComponent } from './component/component.component';
import { BienvenidaComponent } from './components/bienvenida/bienvenida.component';
import { TarjetaCampannaComponent } from './components/tarjeta-campanna/tarjeta-campanna.component';

@NgModule({
  declarations: [
    Login,
    CampannasComponent,
    PrincipalComponent,
    ComponentComponent,
    BienvenidaComponent,
    TarjetaCampannaComponent,
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [ComponentComponent]
})
export class AppModule { }
