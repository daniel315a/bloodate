import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-component',
  templateUrl: './component.component.html',
  styleUrls: ['./component.component.css'],
})
export class ComponentComponent implements OnInit {

  public login: Boolean;

  constructor() {
    localStorage.setItem('token_app', '0cc175b9c0f1b6a831c399e269772661');
    this.login = !(localStorage.getItem('token_seguridad') === null);
  }

  ngOnInit(): void {
  }

}
