import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { parseI18nMeta } from '@angular/compiler/src/render3/view/i18n/meta';

@Injectable()
export class CampannaService{

    public url:string;

    constructor(public _httpClient : HttpClient){
        this.url = 'http://localhost/app_bloodate';
    }

    public consultarUltimaCampanna():Observable<any>
    {
        let token = localStorage.getItem('token_seguridad');
        let solicitud = 'ultima_campanna';
        //let municipio = localStorage.getItem('id_municipio');
        let municipio = 1;

        return this._httpClient.get(this.url + `/Campanna?token=${token}&solicitud=${solicitud}&id_municipio=${municipio}`);
    }

}