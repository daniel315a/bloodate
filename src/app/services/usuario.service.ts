import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { parseI18nMeta } from '@angular/compiler/src/render3/view/i18n/meta';

@Injectable()
export class UsuarioService{

    public url:string;

    constructor(public _httpClient : HttpClient){
        this.url = 'http://localhost/app_bloodate';
    }

    public login(usuario:string, contrasenna:string):Observable<any> {
        let parametros = new FormData();
		parametros.append('token', localStorage.getItem('token_app'));
		parametros.append('nombre_acceso', usuario);
		parametros.append('contrasenna', contrasenna);
        parametros.append('solicitud', 'login');

        return this._httpClient.post(
            this.url + '/Usuario', 
            parametros
        );
    }

    public consultarUsuarioActual():Observable<any>
    {
        let token = localStorage.getItem('token_seguridad');
        let solicitud = 'consultar_por_token';

        return this._httpClient.get(this.url + `/Usuario?token=${token}&solicitud=${solicitud}`);
    }

}