import { ModuleWithProviders, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComponentComponent } from './component/component.component';
import { Login } from './components/login/login.component';
import { PrincipalComponent } from './components/principal/principal.component';

const appRoutes: Routes = [
    {
        path: '',
        component: Component
    },
    {
        path: '**',
        component: Component
    }
];

export const appRoutingProviders: any[] = [];
export const routing:ModuleWithProviders = RouterModule.forRoot(appRoutes);