import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'bienvenida',
  templateUrl: './bienvenida.component.html',
  styleUrls: ['./bienvenida.component.css'],
  providers: [
    UsuarioService
  ]
})
export class BienvenidaComponent implements OnInit {

  public nombreUsuario:string;

  constructor(private _usuarioService : UsuarioService) {
    this.llenarDatosUsuario();
  }

  public llenarDatosUsuario(){
    let respuesta = this._usuarioService.consultarUsuarioActual();
    
    respuesta.subscribe(
      result => {
        if(result.resultado == true) {
          this.nombreUsuario = result.datos.nombres;
        }
      },
      error =>
      {
        console.log(error);
      }
    );
  }

  public logout(){
    localStorage.clear();
    window.location.reload();
  }

  ngOnInit(): void {
  }

}
