import { Component } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    providers: [
        UsuarioService
    ],
})
export class Login {

    public usuario: string;
    public contrasenna: string;

    constructor(private _usuarioService : UsuarioService){
        
    }

    login(){
        let resultado = this._usuarioService.login(this.usuario, this.contrasenna);

        resultado.subscribe(
            result => {
                if(result.resultado == true)
                {
                    let usuario = result.datos;
                    localStorage.setItem('token_seguridad', usuario.token_seguridad);
                    window.location.reload();
                }
                else
                {
                    alert ('Usuario o contraseña inválidos');
                }
            },
            error => {
                console.log(error.error);

                alert ('Usuario o contraseña inválidos');
            }
        );

    }

}