import { Component, OnInit } from '@angular/core';
import { CampannaService } from '../../services/campanna.service';
import { error } from 'protractor';

@Component({
  selector: 'tarjeta-campanna',
  templateUrl: './tarjeta-campanna.component.html',
  styleUrls: ['./tarjeta-campanna.component.css'],
  providers: [
    CampannaService
  ]
})
export class TarjetaCampannaComponent implements OnInit {

  public mostrarMapa : Boolean;
  public mensajeNotificacion : string;
  public urgente : Boolean;
  public nombreEntidad : string;
  public iframeEntidad : string;
  
  constructor(private _campannaService : CampannaService) {
    this.llenarUltimaCampanna();
  }

  public llenarUltimaCampanna(){
    let respuesta = this._campannaService.consultarUltimaCampanna();

    respuesta.subscribe(
      result => {
        console.log(result);

        if(result.resultado == true) {
          let campanna = result.datos[0];
          this.mensajeNotificacion = campanna.mensaje_notificacion;
          this.urgente = campanna.urgente == "1";
          this.mostrarMapa = true;
          this.iframeEntidad = campanna.entidad.iframe_google;
        }
      },
      error => {
        console.log(error);
      }
    );

  }

  ngOnInit(): void {
  }

}
