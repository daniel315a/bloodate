import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TarjetaCampannaComponent } from './tarjeta-campanna.component';

describe('TarjetaCampannaComponent', () => {
  let component: TarjetaCampannaComponent;
  let fixture: ComponentFixture<TarjetaCampannaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TarjetaCampannaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TarjetaCampannaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
