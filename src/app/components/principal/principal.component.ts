import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css'],
  providers: [
    UsuarioService
  ]
})
export class PrincipalComponent implements OnInit {

  constructor(private _usuarioService : UsuarioService) {
  }

  ngOnInit(): void {
  }

}
